from django.shortcuts import render, redirect
from .models import Isian
from .forms import Form_bantuan
from django.http import JsonResponse, HttpResponse
import json

# Create your views here.
def bantuan(request):
    if request.method == "POST":
        form = Form_bantuan(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'haltidak.html')
    else :
        form = Form_bantuan()
    return render(request, 'halscreening.html', {'form': form})

def pendaftar_bantuan(request):
    hasilForm = Isian.objects.all()
    return render(request,'haldaftar.html',{'hasilForm': hasilForm})

def hasil(request):
    data = []
    semua = Isian.objects.all()
    data.append({'Bogor':semua.filter(alamat = 'Bogor').count()})
    data.append({'DKI Jakarta':semua.filter(alamat = 'DKI Jakarta').count()})
    data.append({'Depok':semua.filter(alamat = 'Depok').count()})
    data.append({'Tangerang':semua.filter(alamat = 'Tangerang').count()})
    data.append({'Bekasi':semua.filter(alamat = 'Bekasi').count()})
    return JsonResponse(data,safe=False)