from django.shortcuts import render
from django.urls import path
from . import views

app_name = 'screeningCovid'

urlpatterns = [
    path('screening', views.bantuan, name='screeningCovid'),
    path('daftarbantuan', views.pendaftar_bantuan, name='daftarbantuan'),
    path('daftarbantuan/data', views.hasil, name='hasil'),
    #path('bukti-tf-swab', views.bukti, name='bukti-tf-swab'),
]