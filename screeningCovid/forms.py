from django import forms
from .models import Isian

class Form_bantuan(forms.ModelForm):
    class Meta:
        model = Isian
        fields = ('nama', 'nohp', 'alamat')
        pilihan_provinsi = [
            ('DKI Jakarta', 'DKI Jakarta'),
            ('Tangerang', 'Tangerang'),
            ('Bogor', 'Bogor'),
            ('Depok', 'Depok'),
            ('Bekasi', "Bekasi")
        ]

        widgets = {
            'nama': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder': "Isi dengan nama lengkapmu!"}),

            'nohp': forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': "Isi dengan nomer yang dapat dihubungi!"}),

            'alamat': forms.Select(choices=pilihan_provinsi, attrs={
                'class':'form-control',
                'placeholder': "Isi dengan alamat lengkap!"}),
        }