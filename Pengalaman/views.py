from django.shortcuts import render, redirect
from django.core import serializers
from django.http import HttpResponse
from .forms import FormPengalaman
from .models import Pengalaman

# Create your views here.
def pengalamanPage(request):
    if request.method == 'POST':
        form = FormPengalaman(request.POST)
        if form.is_valid():
            form.save()
            form = FormPengalaman()
        listPengalaman = Pengalaman.objects.all()
        return render(request, 'Pengalaman.html', {'form':form, 'listPengalaman':listPengalaman})
    else:
        form = FormPengalaman()
        listPengalaman = Pengalaman.objects.all()
        return render(request, 'Pengalaman.html', {'form': form, 'listPengalaman':listPengalaman})

def pengalamandata(request):
    data = Pengalaman.objects.filter()
    data_list = serializers.serialize('json', data)
    return HttpResponse(data_list, content_type="text/json-comment-filtered")