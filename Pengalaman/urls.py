from django.urls import path
from django.shortcuts import render

from . import views

app_name = 'Pengalaman'

urlpatterns = [
    path('pengalaman', views.pengalamanPage, name= 'pengalamanPage'),
    path('pengalamandata', views.pengalamandata, name= 'pengalamandata'),
]