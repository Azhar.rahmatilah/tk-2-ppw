$(document).ready(function() {
    $("#input").submit(function() {
        alert("kamu berhasil daftar untuk dijemput!");
    });
    $(".hover").mouseover(function() {
        $(this).css("background-color","#8BA6B9");
    });
    $(".hover").mouseout(function() {
        $(this).css("background-color","white");
    });
    $(".hover").click(function() {
        $(this).hide();
    });
    $("#myBtn").click(function() {
        document.documentElement.scrollTop = 0;
    });
    $("#search").click(function() {
        $("#konten").empty();
  
        get_nama($('input[name=q]').val());
    });
    var dataURL = 'daftarbantuan/data'
    var data = {
        'keys' : [],
        'values' : [],
    }
    $.ajax({
        method : 'GET',
        url : dataURL,
        success:function(response) {
            for (var i in response) {
                var key = Object.keys(response[i])[0]
                var value = Object.values(response[i])[0]
                data.keys.push(key)
                data.values.push(value)
            }
            buildChart()
        }
    })
    function buildChart(){
        zingchart.render({
            id: 'grafik',
            data: {
              "type": "bar",
              "title": {
                  "text": "Jumlah masyarakat butuh bantuan berdasarkan Kota",
                  "fontSize": 18
              },
      
              "plot": {
                  'background-color': "#8BA6B9",
                  "animation": {
                  "delay": "100",
                  "effect": "4",
                  "method": "5",
                  "sequence": "1"
                  }
               },
              
              "scale-x": {
                  "values" : data.keys
              },
              series : [
                  {
                    "values" : data.values
                  },

              ] 
            }
        });
    }
    
});

