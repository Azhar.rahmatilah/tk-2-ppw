from django.shortcuts import render, redirect
from django.db.models import Sum
from django.conf import settings
from django.conf.urls.static import static

from .forms import FormDonatur
from .models import Donatur
# Create your views here.

choices = [
    ('1', 'Aceh'),
    ('2', 'Sumatera Utara'),
    ('3', 'Sumatera Barat'),
    ('4', 'Riau'),
    ('5', 'Kepulauan Riau'),
    ('6', 'Bengkulu'),
    ('7', 'Jambi'),
    ('8', 'Sumatera Selatan'),
    ('9', 'Kepulauan Bangka Belitung'),
    ('10', 'Lampung'),
    ('11', 'Banten'),
    ('12', 'Jawa Barat'),
    ('13', 'Dki Jakarta'),
    ('14', 'Jawa Tengah'),
    ('15', 'Di Yogyakarta'),
    ('16', 'Jawa Timur'),
    ('17', 'Bali'),
    ('18', 'Nusa Tenggara Barat'),
    ('19', 'Nusa Tenggara Timur'),
    ('20', 'Kalimantan Timur'),
    ('21', 'Kalimantan Barat'),
    ('22', 'Kalimantan Selatan'),
    ('23', 'Kalimantan Tengah'),
    ('24', 'Kalimantan Utara'),
    ('25', 'Gorontalo'),
    ('26', 'Sulawesi Barat'),
    ('27', 'Sulawesi Tengah'),
    ('28', 'Sulawesi Selatan'),
    ('29', 'Sulawesi Tenggara'),
    ('30', 'Sulawesi Utara'),
    ('31', 'Maluku'),
    ('32', 'Maluku Utara'),
    ('33', 'Papua Barat'),
    ('34', 'Papua'),
]
CHOICES_CLEANED = []
for i in choices:
    CHOICES_CLEANED.append(i[1])


def donatur(request):
    total = Donatur.objects.all()
    sum = 0
    for item in total :
        sum += item.getNominal()
    return render(request, 'donasi.html', {'jumlah': sum, 'total': total})

def donasi(request):
    total = Donatur.objects.all()
    sum = 0
    for item in total :
        sum += item.getNominal()
    if request.method == 'POST':
        donatur_form = FormDonatur(request.POST,request.FILES or None)
        if donatur_form.is_valid():
            donatur_form.save()
            return redirect('../')
    else:
        donatur_form = FormDonatur()
    return render(request, "formDonasi.html", {'listProvinsi': CHOICES_CLEANED, 'data_form': donatur_form, 'jumlah': sum})