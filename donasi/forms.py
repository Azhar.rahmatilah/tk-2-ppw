from django import forms
from donasi.models import Donatur


class FormDonatur(forms.ModelForm):
    class Meta:
        model = Donatur
        fields = [
            'nama_donatur',
            'nominal_donatur',
            'kota_donatur',
            'pesan_donatur',
            'bukti_donatur',
        ]

        widgets = {
            'nominal_donatur': forms.NumberInput(),
            'kota_pelamar': forms.TextInput()
        }

    def __init__(self, *args, **kwargs):
        super(FormDonatur, self).__init__(*args, **kwargs)
        self.fields['nama_donatur'].widget.attrs['style'] = 'border-radius: 20px; margin:auto;'
        self.fields['nominal_donatur'].widget.attrs['style']  = 'border-radius: 20px;margin:auto'
        self.fields['pesan_donatur'].widget.attrs['style'] = 'border-radius: 20px; margin:auto;'

        self.fields['kota_donatur'].widget.attrs['style'] = 'border-radius: 20px; margin:auto;'

        self.fields['kota_donatur'].widget.attrs['id'] = 'kota_pelamar'

        self.fields['nama_donatur'].widget.attrs.update({'placeholder': 'Masukkan Nama Anda', 'class':'text-center form-control'})
        self.fields['nominal_donatur'].widget.attrs.update({'placeholder': 'Masukkan Nominal Donasi', 'class':'text-center form-control'})
        self.fields['pesan_donatur'].widget.attrs.update({'placeholder': 'Masukan Pesan Anda', 'class':'text-center form-control'})