from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'donasi'

urlpatterns = [
    path('', views.donatur, name='donatur'),
    path('yuk/', views.donasi, name='donasi'),
]