from django.db import models

# Create your models here.

class Donatur(models.Model):
    PROVINSI = (
        ('1', 'Aceh'),
        ('2', 'Sumatera Utara'),
        ('3', 'Sumatera Barat'),
        ('4', 'Riau'),
        ('5', 'Kepulauan Riau'),
        ('6', 'Bengkulu'),
        ('7', 'Jambi'),
        ('8', 'Sumatera Selatan'),
        ('9', 'Bangka Belitung'),
        ('10', 'Lampung'),
        ('11', 'Banten'),
        ('12', 'Jawa Barat'),
        ('13', 'Jakarta'),
        ('14', 'Jawa Tengah'),
        ('15', 'Di Yogyakarta'),
        ('16', 'Jawa Timur'),
        ('17', 'Bali'),
        ('18', 'Nusa Tenggara Barat'),
        ('19', 'Nusa Tenggara Timur'),
        ('20', 'Kalimantan Timur'),
        ('21', 'Kalimantan Barat'),
        ('22', 'Kalimantan Selatan'),
        ('23', 'Kalimantan Tengah'),
        ('24', 'Kalimantan Utara'),
        ('25', 'Gorontalo'),
        ('26', 'Sulawesi Barat'),
        ('27', 'Sulawesi Tengah'),
        ('28', 'Sulawesi Selatan'),
        ('29', 'Sulawesi Tenggara'),
        ('30', 'Sulawesi Utara'),
        ('31', 'Maluku'),
        ('32', 'Maluku Utara'),
        ('33', 'Papua Barat'),
        ('34', 'Papua'),
    )
    nama_donatur = models.CharField(max_length=100)
    nominal_donatur = models.IntegerField()
    provinsi_donatur = models.CharField(
        max_length=2, 
        choices=PROVINSI, 
        blank=True)
    kota_donatur = models.CharField(max_length=15, blank=True)
    pesan_donatur = models.CharField(max_length=100)
    bukti_donatur = models.ImageField(upload_to= 'donatur/', height_field=None, width_field=None, max_length=100, null=True)

    def getNominal(self):
        return self.nominal_donatur

    def getNama(self):
        return self.nama_donatur

    def __str__(self):
        return "{}. {}".format(self.id, self.nama_donatur)
