from django.test import TestCase, Client
from django.urls import resolve
from .views import list_rs, list_pendaftar #,bukti
from .models import Pendaftar
from django.core.files.uploadedfile import SimpleUploadedFile
import tempfile
from django.http import HttpRequest

class list_rumah_sakit(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/list_rumah_sakit')
        self.assertEqual(response.status_code, 200)

    def test_listrs_using_index_func(self):
        found = resolve('/list_rumah_sakit')
        self.assertEqual(found.func, list_rs)

    def test_template_used(self):
        response =Client().get('/list_rumah_sakit')
        self.assertTemplateUsed(response, 'RumahSakit.html')
    
    # def test_html_contains(self):
    #     request = HttpRequest()
    #     response = list_rs(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Rumah Sakit Rujukan', html_response)
    #     self.assertIn('Pendaftaran SWAB Test', html_response)
    #     self.assertIn('transfer ke rekening berikut', html_response)
    #     self.assertIn('*Lakukan pembayaran terlebih dahulu (min 50%)', html_response)
    #     self.assertIn('Nama', html_response)
    #     self.assertIn('Tanggal', html_response)
    #     self.assertIn('Jam', html_response)
    #     self.assertIn('Nomor HP', html_response)
    #     self.assertIn('Bukti Transfer', html_response)

    def test_daftar_swab(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        pendaftar = Pendaftar(nama='iqfal', telp='081298773221', rumah_sakit='RSUD Tarakan - RP850.000',
                              tanggal='2020-10-23', jam='15.00 WIB', Bukti_Transfer=image)
        pendaftar.save()
        pendaftar2 = Pendaftar.objects.create(nama='iqfalll', telp='081298773221', rumah_sakit='RSUD Tarakan - RP850.000',
                              tanggal='2020-10-23', jam='15.00 WIB', Bukti_Transfer=image)
        self.assertEqual(Pendaftar.objects.all().count(), 2)
    
    def test_daftar_swab_url_is_exist(self):
        response = Client().post('/list_rumah_sakit', data = {'nama':'iqfal', 'telp':'081298773221', 'rumah_sakit':'RSUD Tarakan - RP850.000',
                                                       'tanggal':'2020-10-23', 'jam':'15.00 WIB', 'Bukti_Transfer':'image'})
        self.assertEqual(response.status_code, 200)

class list_data_pendaftar(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/pendaftar')
        self.assertEqual(response.status_code, 200)

    def test_list_pendaftar_using_index_func(self):
        found = resolve('/pendaftar')
        self.assertEqual(found.func, list_pendaftar)

    def test_template_used(self):
        response = Client().get('/pendaftar')
        self.assertTemplateUsed(response, 'listdaftar.html')

    def test_html_contains(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        pendaftar = Pendaftar.objects.create(nama='iqfal', telp='081298773221', rumah_sakit='RSUD Tarakan - RP850.000',
                              tanggal='2020-10-23', jam='15.00 WIB', Bukti_Transfer=image)
        request = HttpRequest()
        response = list_pendaftar(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Nama', html_response)
        self.assertIn('Nama', html_response)
        self.assertIn('Nomor', html_response)
        self.assertIn('Rumah Sakit', html_response)
        self.assertIn('Tanggal', html_response)
        self.assertIn('Jam', html_response)
        self.assertIn('iqfal', html_response)
        self.assertIn('Bukti Transfer', html_response)
        self.assertIn('81298773221', html_response)
        self.assertIn('RSUD Tarakan', html_response)
        self.assertIn('Oct. 23, 2020', html_response)
        self.assertIn('15.00', html_response)