from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve
from .views import register, login_account, logout_account
from django.http import HttpRequest
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User

class register_account(TestCase):

    def test_story9_using_index_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_template_used(self):
        response =Client().get('/register/')
        self.assertTemplateUsed(response, 'register.html')
    
    def test_html_contains(self):
        request = HttpRequest()
        response = register(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Username', html_response)
        self.assertIn('Email', html_response)
        self.assertIn('Password', html_response)
        self.assertIn('Password', html_response)

    def test_url_is_exist(self):
    	response=Client().get('/register/')
    	self.assertEqual(response.status_code, 200)

class login_page(TestCase):
	def setUp(self):
		self.user = {
		'username' : 'testing',
		'password' : 'coba12345'
		}
		User.objects.create_user(**self.user)
	def test_login(self):
		response = self.client.post('/login/', self.user, follow = True)
		self.assertTrue(response.context['user'].is_active)

	def test_login_page_index_func(self):
		found = resolve('/login/')
		self.assertEqual(found.func, login_account)

	def test_url_is_exist(self):
		response=Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_template_used(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'loginaccount.html')

	def text_html_contains(self):
		request = HttpRequest()
		response = login_account(request)
		html_response = response.content.decode('utf8')
		self.assertIn("Username", html_response)
		self.assertIn("Password", html_response)

class logout_test(TestCase):

	def test_login_page_index_func(self):
		found = resolve('/logout/')
		self.assertEqual(found.func, logout_account)

	def test_url_is_exist(self):
		response=Client().get('/logout/')
		self.assertEqual(response.status_code, 302)
