from django.shortcuts import render, redirect 
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import *
from .forms import registerAccount

# Create your views here.
def register(request):
	form = registerAccount()
	if request.method == 'POST':
		form = registerAccount(request.POST)
		if form.is_valid():
			form.save()
			return redirect('login:login')

	context = {'form':form}
	return render(request, 'register.html', context)

def login_account(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username, password=password)
		if user is not None:
			login(request, user)
			return redirect('home:home')
		else:
			messages.info(request, "username or password invalid")

	return render(request, 'loginaccount.html')

def logout_account(request):
	logout(request)
	return redirect('home:home')