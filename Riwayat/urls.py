from django.shortcuts import render

from django.urls import path

from . import views

app_name = 'Riwayat'

urlpatterns = [
    path('list_riwayat', views.list_riwayat, name='list_riwayat'),
    path('form_riwayat', views.form_riwayat, name='form_riwayat'),
    path('resultdata', views.resultData, name='resultdata')
]
