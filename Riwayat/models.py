from django.db import models

# Create your models here.
class Riwayat(models.Model): 
    nama = models.CharField('Nama', max_length = 30) 
    alamat = models.CharField('Kota / Kabupaten', max_length = 100, default = '')
    tempat = models.CharField('Tempat Dikunjungi', max_length = 100)
    tanggal = models.DateField('Tanggal', auto_now=False, auto_now_add=False, null=True)
    last_modified = models.DateTimeField(auto_now_add = True) 
   
        # renames the instances of the model 
        # with their title name 
    def __str__(self): 
        return self.nama
