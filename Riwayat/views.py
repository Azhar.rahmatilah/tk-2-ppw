from django.shortcuts import render, redirect
from .forms import Form_Riwayat
from .models import Riwayat
from django.conf import settings
from django.conf.urls.static import static
from django.http import JsonResponse

# Create your views here.
def form_riwayat(request):
    if request.method == 'POST':
        form = Form_Riwayat(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('Riwayat:form_riwayat')
    else:
        form = Form_Riwayat()
    return render(request, 'form_riwayat.html', {'form': form})

def list_riwayat(request):
    hasilForm = Riwayat.objects.all().order_by('-tanggal')
    return render(request, 'list_riwayat.html', {'hasilForm': hasilForm })

def resultData(request):
    votedata = []
    riwayat = Riwayat.objects.all()
    itung = riwayat.filter(alamat = 'Jakarta Selatan').count()
    votedata.append({'Jakarta Selatan':itung})
    itung1 = riwayat.filter(alamat = 'Jakarta Timur').count()
    votedata.append({'Jakarta Timur':itung1})
    itung2 = riwayat.filter(alamat = 'Jakarta Utara').count()
    votedata.append({'Jakarta Utara':itung2})
    itung3 = riwayat.filter(alamat = 'Jakarta Barat').count()
    votedata.append({'Jakarta Barat':itung3})
    itung4 = riwayat.filter(alamat = 'Jakarta Pusat').count()
    votedata.append({'Jakarta Pusat':itung4})
    itung5 = riwayat.filter(alamat = 'Kepulauan Seribu').count()
    votedata.append({'Kepulauan Seribu':itung5})
    return JsonResponse(votedata, safe=False)