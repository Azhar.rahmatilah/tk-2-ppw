from django import forms 
from .models import Riwayat

class DateInput(forms.DateInput):
    input_type = 'date'
  
class Form_Riwayat(forms.ModelForm):  
    class Meta: 
        model = Riwayat
        fields = ('nama', 'alamat', 'tempat', 'tanggal')
        pilihan_alamat = [
            ('Kepulauan Seribu' , 'Kepulauan Seribu'),
            ('Jakarta Selatan', 'Jakarta Selatan'),
            ('Jakarta Timur', 'Jakarta Timur'),
            ('Jakarta Barat', 'Jakarta Barat'),
            ('Jakarta Utara', 'Jakarta Utara'),
            ('Jakarta Pusat', 'Jakarta Pusat'),  
        ]
        
        attrs = {
            'class' : 'class-form'
        }

        widgets={
            'nama' : forms.TextInput(attrs={'class' : 'class-form', 'aria-describedby' : 'emailHelp', 'placeholder' : 'Masukkan nama anda'}),
            'alamat' : forms.Select(choices=pilihan_alamat, attrs={'class' : 'class-form'}) ,
            'tempat' : forms.TextInput(attrs={'class' : 'class-form','placeholder' : 'Tempat apa yang dikunjungi di kota / kabupaten tersebut?'}),
            'tanggal' : DateInput(attrs={'class' : 'class-form'}),   
        }