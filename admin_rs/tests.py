from django.test import TestCase, Client
from django.urls import resolve
from .views import pilihan_admin
from django.http import HttpRequest

# class login_test(TestCase):

    # def test_url_is_exist(self):
    #     response = Client().get('/admin-rs/login')
    #     self.assertEqual(response.status_code, 200)

    # def test_login_using_index_func(self):
    #     found = resolve('/admin-rs/login')
    #     self.assertEqual(found.func, login)

    # def test_template_used(self):
    #     response =Client().get('/admin-rs/login')
    #     self.assertTemplateUsed(response, 'login.html')

    # def test_template_used(self):
    #     response =Client().get('/admin-rs/login-gagal')
    #     self.assertTemplateUsed(response, 'loginsalah.html')
    
    # def test_html_contains_login(self):
    #     request = HttpRequest()
    #     response = login(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Gacoor Admin', html_response)
    #     self.assertIn('Username', html_response)
    #     self.assertIn('Login', html_response)

    # def test_html_contains_login_gagal(self):
    #     request = HttpRequest()
    #     response = login_gagal(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Gacoor Admin', html_response)
    #     self.assertIn('Username', html_response)
    #     self.assertIn('Login', html_response)
    #     self.assertIn('Invalid login! please try again', html_response)

class after_login(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/admin-rs/pilihan-admin')
        self.assertEqual(response.status_code, 200)

    def test_login_using_index_func(self):
        found = resolve('/admin-rs/pilihan-admin')
        self.assertEqual(found.func, pilihan_admin)

    def test_template_used(self):
        response =Client().get('/admin-rs/pilihan-admin')
        self.assertTemplateUsed(response, 'pilihan-admin.html')

    # def test_html_contains_pilihan_admin(self):
    #     request = HttpRequest()
    #     response = pilihan_admin(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Pendaftar SWAB Test', html_response)
    #     self.assertIn('Masyarakat Butuh Bantuan', html_response)

    # def test_html_contains_login_berhasil(self):
    #     request = HttpRequest()
    #     response = cobalogin(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Pendaftar SWAB Test', html_response)
    #     self.assertIn('Masyarakat Butuh Bantuan', html_response)
        # self.assertIn('Login', html_response)
        # self.assertIn('Invalid login! please try again', html_response)
#         self.assertIn('Nama', html_response)
#         self.assertIn('Tanggal', html_response)
#         self.assertIn('Jam', html_response)
#         self.assertIn('Nomor HP', html_response)
#         self.assertIn('Bukti Transfer', html_response)

    # def test_login_salah(self):
    #     image = tempfile.NamedTemporaryFile(suffix=".jpg").name
    #     pendaftar = Pendaftar(nama='iqfal', telp='081298773221', rumah_sakit='RSUD Tarakan - RP850.000',
    #                           tanggal='2020-10-23', jam='15.00 WIB', Bukti_Transfer=image)
    #     pendaftar.save()
    #     pendaftar2 = Pendaftar.objects.create(nama='iqfalll', telp='081298773221', rumah_sakit='RSUD Tarakan - RP850.000',
    #                           tanggal='2020-10-23', jam='15.00 WIB', Bukti_Transfer=image)
    #     self.assertEqual(Pendaftar.objects.all().count(), 2)
    
#     def test_daftar_swab_url_is_exist(self):
#         response = Client().post('/list_rumah_sakit', data = {'nama':'iqfal', 'telp':'081298773221', 'rumah_sakit':'RSUD Tarakan - RP850.000',
#                                                        'tanggal':'2020-10-23', 'jam':'15.00 WIB', 'Bukti_Transfer':'image'})
#         self.assertEqual(response.status_code, 200)

# class list_data_pendaftar(TestCase):
#     def test_url_is_exist(self):
#         response = Client().get('/pendaftar')
#         self.assertEqual(response.status_code, 200)

#     def test_list_pendaftar_using_index_func(self):
#         found = resolve('/pendaftar')
#         self.assertEqual(found.func, list_pendaftar)

#     def test_template_used(self):
#         response = Client().get('/pendaftar')
#         self.assertTemplateUsed(response, 'listdaftar.html')

#     def test_html_contains(self):
#         image = tempfile.NamedTemporaryFile(suffix=".jpg").name
#         pendaftar = Pendaftar.objects.create(nama='iqfal', telp='081298773221', rumah_sakit='RSUD Tarakan - RP850.000',
#                               tanggal='2020-10-23', jam='15.00 WIB', Bukti_Transfer=image)
#         request = HttpRequest()
#         response = list_pendaftar(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('Nama', html_response)
#         self.assertIn('Nama', html_response)
#         self.assertIn('Nomor', html_response)
#         self.assertIn('Rumah sakit', html_response)
#         self.assertIn('Tanggal', html_response)
#         self.assertIn('Jam', html_response)
#         self.assertIn('iqfal', html_response)
#         self.assertIn('81298773221', html_response)
#         self.assertIn('RSUD Tarakan', html_response)
#         self.assertIn('Oct. 23, 2020', html_response)
#         self.assertIn('15.00', html_response)